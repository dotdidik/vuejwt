import Vue from 'vue'
import Router from 'vue-router'
import pageLogin from '@/components/pageLogin'
import pageRegister from '@/components/pageRegister'
import helloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'pageLogin',
      component: pageLogin
    },
    {
      path: '/register',
      name: 'pageRegister',
      component: pageRegister
    },
    {
      path: '/hello',
      name: 'helloWorld',
      component: helloWorld
    }
  ]
})
